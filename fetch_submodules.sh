#!/usr/bin/env bash

echo "Fetching all submodules recursively...
Note: All submodules are added and fetched using https protocol, in case you are using ssh, you may be asked to provide the https credentials to fetch the submodules"
git submodule update --init --recursive
