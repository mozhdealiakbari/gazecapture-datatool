import random
import argparse
import os
import inspect
import sys
import cv2

cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.join(cur_path, '..'))
from common.plot_annotations.plot import plotUtils
from datatool_api.config.APIConfig import DTAPIConfig
from datatool_api.models.DTDataset import DTDataset
from datatool_api.models.ImageSample import ImageSample
from datatool_api.models.Categories import Categories
from common.overview_collage.generate_overview_collage import generate_overview_tiles
from custom_dataset_model import DTDatasetCustom
from custom_base_types import *

"""
Note: For the development purpose, developer can use the relative imports with ..common.plot, ..datatool_api.models 
      to enable auto-suggestions in IDE for the plotUtils, DTDataset and ImageSample APIs . If used, please remember to 
      remove the relative imports before committing this script as this script as standalone will not work due to 
      relative imports from inside the project. 
"""


class VisualizeAnnotations:

    @staticmethod
    def plot(input_dir: str, sample: GazeImageData, output_dir: str):
        image_name = sample.frames
        output_path = os.path.join(output_dir, image_name )
        image = cv2.imread(os.path.join(input_dir, image_name ))
        #height, width = image.shape[:2]
        plotter = plotUtils(image)
        # Plot Eye open/Close
        

        
		
        face = {'x': sample.appleFace.X, 'y':sample.appleFace.Y, 'w': sample.appleFace.W, 'h': sample.appleFace.H}
        LeftEye = {'x': sample.appleLeftEye.X+sample.appleFace.X, 'y':sample.appleLeftEye.Y+sample.appleFace.Y, 'w': sample.appleLeftEye.W, 'h': sample.appleLeftEye.H}
		
        RightEye = {'x': sample.appleRightEye.X+sample.appleFace.X, 'y':sample.appleRightEye.Y+sample.appleFace.Y, 'w': sample.appleRightEye.W, 'h': sample.appleRightEye.H}
		
        if (sample.appleFace.isValid == 1):
            plotter.plot_bbox(face, color=(0, 255, 0), thickness=3)
            if (sample.appleRightEye.isValid == 1):
              XCnt_Right = sample.appleRightEye.X+(sample.appleFace.X) + (sample.appleRightEye.W)/2
              YCnt_Right = (sample.appleFace.Y + sample.appleRightEye.Y) + (sample.appleRightEye.H)/2
              			  
              plotter.plot_bbox(RightEye, color=(0, 0, 255), thickness=2)

              if (sample.screen.Orientation == 1):
                plotter.plot_gaze_angles((int(XCnt_Right),int(YCnt_Right)),sample.dotInfo.XCam , -abs(sample.dotInfo.YCam), magnitude=100, color=(255, 0, 0), thickness=1)
              else:
                plotter.plot_gaze_angles((int(XCnt_Right),int(YCnt_Right)),sample.dotInfo.XCam , sample.dotInfo.YCam, magnitude=100, color=(255, 0, 0), thickness=1)
            if (sample.appleLeftEye.isValid == 1):
              XCnt_Left = sample.appleLeftEye.X+(sample.appleFace.X) + (sample.appleLeftEye.W)/2
              YCnt_Left = (sample.appleFace.Y + sample.appleLeftEye.Y) + (sample.appleLeftEye.H)/2
              plotter.plot_bbox(LeftEye, color=(0, 0, 255), thickness=2)
              if (sample.screen.Orientation == 1):
                plotter.plot_gaze_angles((int(XCnt_Left),int(YCnt_Left)),sample.dotInfo.XCam , -abs(sample.dotInfo.YCam), magnitude=100, color=(255, 0, 0), thickness=2)
              else:
                plotter.plot_gaze_angles((int(XCnt_Left),int(YCnt_Left)),sample.dotInfo.XCam , abs(sample.dotInfo.YCam), magnitude=100, color=(255, 0, 0), thickness=2)

        
        #plotter.plot_gaze_angles(None,sample.dotInfo.XCam , sample.dotInfo.YCam, magnitude=100, color=(255, 0, 0), thickness=1)

         

        # Finally save the sample
        plotter.save(output_path)

    @staticmethod
    def run(input_dir, output_dir, sample_count, operation_mode):
        os.makedirs(output_dir, exist_ok=True)
        DTAPIConfig.disable_validation()
        dataset = DTDatasetCustom(name='dataset', operatingMode=operation_mode).load_from_json(os.path.join(
            input_dir, 'dataset.json'), element_list=['images'])
        if dataset.images.item_count() < sample_count:
            raise Exception('Required sample count is more than samples available in input json')

        indices = random.sample(range(dataset.images.item_count()), sample_count)
        sample_ids = list(dataset.images.keys())
        for index in indices:
            sample = dataset.images.pop(sample_ids[index])
            VisualizeAnnotations.plot(os.path.join(input_dir, 'sample_files'), sample, output_dir)

        print('Plotting for randomly drawn samples completed')


def main():
    parser = argparse.ArgumentParser(description='Visualize the annotations on randomly drawn sample from datatool '
                                                 'output')
    parser.add_argument('--input-dir', '-i', help='Input directory where datatool output is stored', required=True)
    parser.add_argument('--output-dir', '-o', help='Output directory', required=True)
    parser.add_argument('--operation-mode', '-om', choices=['memory', 'disk', 'ramdisk'], default='memory',
                        help='Operation mode for the datatool to load and process the datatool output')
    parser.add_argument('--sample-count', '-s', help='Number of samples', default=100, type=int)
    parser.add_argument('--random-seed', '-rs', type=int, help='Random seed value, provide for reproducibility')

    args = parser.parse_args()
    random.seed(args.random_seed)
    VisualizeAnnotations.run(args.input_dir, args.output_dir, args.sample_count, args.operation_mode)
    generate_overview_tiles(args.output_dir, args.random_seed, os.path.join(args.output_dir, 'collage'))


if __name__ == '__main__':
    main()
